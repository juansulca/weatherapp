package com.jsulca.weatherapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherService {

    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Path("api_version") version: String = "2.5",
        @Query("q") city: String,
        @Query("appid") appId: String = "1fcbdd2f0b51b574fb05897c09f6e1a5"
    ) : Call<WeatherResponse>

    companion object {
        val instance: WeatherService by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}